## User Model and related information

# User model
    id              String              Required!
    email           String              Required!
    job title       String              Required!
    first name      String              ( Optional )
    last name       String              ( Optional )
    image url       String              ( Optional )
    phone-number    String              ( Optional )
    created-at      Date                Required!
    updated-at      Date                Required!