## Division Model and related information

# Division Model:
    id              String              Required!
    name            String              Required!
    employees       User []             Required!
    Managers        User []             Required! ( At least one )
    About           String              ( Optional )
    Projects        Project []          ( Optional )
    created-at      Date                Required!
    updated-at      Date                Required!