## Company Model and related information

# Company model

    id              String              Required!
    name            String              Required!
    divisions       Division []         ( Optional )
    employees       User []             ( Optional )
    created-at      Date                Required!
    updated-at      Date                Required!