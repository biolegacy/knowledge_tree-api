## Skill Model and related information

# Skill Model

    id              String              Required!
    title           String              Required!
    description     String              Required!
    tag             String []           ( Optional )
    created-at      Date                Required!
    updated-at      Date                Required!