## Request Model and related information

# Request Model

    id              String              Required!
    title           String              Required!
    owner           User                Required!
    interest        Skill []            ( Optional )
    description     String              ( Optional )
    created-at      Date                Required!
    updated-at      Date                Required!