## Project Model and Related information

# Project Model:
    id              String          Required!
    title           String          Required!
    owner           Division        ( Optional )
    tech-stack      Skill []        ( Optional )
    members         User []         ( Optional )
    deadline        Date            ( Optional )
    created-at      Date            Required!
    updated-at      Date            Required!

# Details

