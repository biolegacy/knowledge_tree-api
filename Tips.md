## Useful commands to rebuild the container image etc
    docker-compose build --no-cache
    docker-compose up -d // without Debug Log
    docker-compose up // With Debug Log

## Mongo DB Shell commands reference Link:

    https://docs.mongodb.com/manual/reference/mongo-shell/

## How to Access to Mongo DB shell

    Run the following commands:

    1.  Check the containers list
        
        ```
        docker containers ls
        ```
        
    2.  Make sure mongo is listed as container
    
    3.  Access to the mongo container with following command:
        mongo in the following command refers to the name of the container, 
        it could be different if you changed the name of the container and upload it to the docker container list
        
        ```
        docker exec -it mongo bash
        ```

    4.  When you are in the container you can access to the MongoDB shell with the following command:
        mongo

    5.  Terminal should have changed if you got into the MongoDB shell. You can test if you are in the shell or not with the following command:
        Following command shows you all the database you currently have in the MongoDB
        
        ```
        show dbs
        ```
## Tips and Trick on writing mongoose's `populate()` properly
    Use the following link as reference: 

    https://stackoverflow.com/questions/28179720/mongoose-populate-nested-array/28180427

    path - referring to the field name
    model - referring the name of the schema registered as
     
    ( If you go to random  Schema for example User. You will at the very bottom there is a line mongoose.model("User", UserSchema))
    UserSchema is registered under the "User" keyword. 