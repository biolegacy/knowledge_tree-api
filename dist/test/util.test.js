"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const util_1 = require("./util");
test('Should print out a text', () => {
    const text = util_1.generateText("Edward", 15);
    expect(text).toBe('Edward (15 years old)');
});
test('Should output data-less text', () => {
    const text = util_1.generateText("", undefined);
    expect(text).toBe(' ( years old)');
});
