"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.generateText = (name, age) => {
    return `${name} (${age} years old)`;
};
