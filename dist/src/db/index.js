"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
exports.Init = () => {
    mongoose_1.default.connect(`mongodb://localhost:27017/knowledge-tree-local`, { useNewUrlParser: true })
        .then(() => {
        console.log("Connected to MongoDB!");
    })
        .catch((e) => {
        console.log("Error: ", e);
    });
};
