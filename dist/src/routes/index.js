"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const router = express_1.Router();
router.get('/hello', (req, res) => {
    try {
        res.send("Hello");
    }
    catch (e) {
        res.status(400).send("Bad request");
    }
});
exports.default = router;
