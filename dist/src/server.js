"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const apollo_server_express_1 = require("apollo-server-express");
const graphql_depth_limit_1 = __importDefault(require("graphql-depth-limit"));
const DB = __importStar(require("./db"));
const http_1 = require("http");
const compression_1 = __importDefault(require("compression"));
const cors_1 = __importDefault(require("cors"));
const schema_1 = __importDefault(require("./graphQL/schema"));
const routes_1 = __importDefault(require("./routes"));
const app = express_1.default();
DB.Init();
const server = new apollo_server_express_1.ApolloServer({
    schema: schema_1.default,
    validationRules: [graphql_depth_limit_1.default(7)],
});
app.use('*', cors_1.default());
app.use(compression_1.default());
app.use('/api', routes_1.default);
server.applyMiddleware({ app, path: '/graphql' });
const httpServer = http_1.createServer(app);
httpServer.listen({ port: 3000 }, () => console.log(`\n🚀      GraphQL is now running on http://localhost:3000/graphql`));
