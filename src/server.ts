// Imports
import express from 'express';
import bodyParser from "body-parser";
import { ApolloServer } from 'apollo-server-express';
import depthLimit from 'graphql-depth-limit';
import * as DB from "./db";
import { createServer } from 'http';
import compression from 'compression';
import cors from 'cors';
import schema from './graphQL/schema';
import Routes from "./routes";
// End of imports

const app = express();

// Initialize DB connection
DB.Init();

const server = new ApolloServer({
  schema,
  validationRules: [depthLimit(7)],
});

app.use(bodyParser.urlencoded({
  extended:true
}));
app.use(bodyParser.json());

app.use('*', cors());
app.use(compression());

// Add routes handler
app.use('/api', Routes);

server.applyMiddleware({ app, path: '/graphql' });

const httpServer = createServer(app);

httpServer.listen(
  { port: 8080 },
  (): void => console.log(`\n🚀      GraphQL is now running on http://localhost:3000/graphql`)
);
