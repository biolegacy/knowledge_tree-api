import mongoose, { Schema } from "mongoose";

const SkillSchema = new Schema({
    title:          String!,
    description:   String,
    tag:            [String]!,
    createdAt:      { type: Date!, default: Date.now },
    updatedAt:      { type: Date!, default: Date.now }
});

export default mongoose.model('Skill', SkillSchema);