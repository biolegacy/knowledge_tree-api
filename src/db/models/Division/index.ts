import mongoose from "mongoose";
const Schema = mongoose.Schema;

const divisionStatus = {
    active: "active",
    inactive: "inactive"
}

const DivisionSchema = new Schema({
    name:           { type: String!, unique: true},
    description:    String,
    people:{
        employees:      [{ type: Schema.Types.ObjectId, name: String, ref: 'User' }]!,
        managers:       [{ type: Schema.Types.ObjectId, name: String, ref: 'User' }]!,
        owner:          { type: Schema.Types.ObjectId, ref: 'User'},
    },
    projects:       [{ type: Schema.Types.ObjectId, name: String, ref: 'Project' }]!,
    status:         { type: String, default: divisionStatus.active },
    deactivatedAt:  { type: Date! },
    createdAt:      { type: Date!, default: Date.now },
    updatedAt:      { type: Date!, default: Date.now }
});

export const DivisionStatus = divisionStatus;
export default mongoose.model('Division', DivisionSchema);