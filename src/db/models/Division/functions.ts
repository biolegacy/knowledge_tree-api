import Division, { DivisionStatus } from ".";

export default {
    getDivisions: async () => {
        const query = Division.find({});
        const result = new Promise((resolve, reject) => {
            query.exec(function(err, docs){
                if(err)
                    reject(err);
                resolve(docs);
            });
        });
        return result;
    },
    findByDivisionId: async (id: String):Promise<any> => {
        const query = Division.findById(id).populate('owner');
        const result = new Promise((resolve, reject) => {
            query.exec(function(err, docs){
                if(err)
                    reject(err);
                resolve(docs);
            });
        })
        return result;
    },
    findDivisionByName: async (name:String): Promise<any> => {
        const query = Division.find({ name }).populate('owner');
        const result = new Promise((resolve, reject) => {
            query.exec(function(err, docs){
                if(err)
                    reject(err);
                resolve(docs);
            })
        })
        return result;
    },
    createDivisionREST: async (data:any) => {
        const newDivision = new Division({
            name: data.name,
            about: data.about
        });

        const _result = newDivision.save();
        return _result;
    },
    createDivisionGraphQL: async (args: any) => {
        const newDivision = Division.create({ 
            name: args.name, 
            description: args.description 
        });
        return newDivision;
    },
    updateDivision: async (doc:any, args: any) => {
        // Update values
        doc.name = args.name ? args.name : doc.name;
        doc.description = args.description ? args.escription : doc.description;
        doc.employees = args.employees ? args.employees : doc.employees;
        doc.managers = args.managers ? args.managers : doc.managers;
        doc.projects = args.projects ? args.projects : doc.projects;
        doc.updatedAt = Date.now();

        const result = doc.save();
        return result;
    },
    deactivateDivision: async (doc: any ) => {
        doc.status = DivisionStatus.inactive;
        doc.deactivatedAt = Date.now();
        doc.updatedAt = Date.now();
        doc.save();
        return doc;
    },
    reactivateDivision: async (doc: any ) => {
        doc.status = DivisionStatus.active;
        doc.deactivatedAt = "";
        doc.updatedAt = Date.now();
        doc.save();
        return doc;
    }
}