import Project, { ProjectStates } from ".";

export default {
    getProjects: async (belongsTo?: any): Promise<any> => {
        let query: any;
        if(belongsTo){
            // If division id is provided get all the projects belongs to the Division
            query = Project.find().where({ belongsTo }).populate({
                path: 'owner',
                model: 'User'
            }).populate({
                path: 'belongsTo',
                model: 'Division'
            });
        } else {
            // if division id is not provided get all the projects without any filter
            query = Project.find().populate({
                path: 'owner',
                model: 'User'
            }).populate({
                path: 'belongsTo',
                model: 'Division'
            });
        }
        const result = new Promise((resolve, reject) => {
            query.exec((err: any, docs: any) => {
                if(err)
                    reject(err);
                resolve(docs);
            })
        })
        return result;
    },
    findProjectById: async ( args: any ): Promise<any> => {
        const query = Project.findById( args.id ).populate({
            path: 'owner',
            model: 'User'
        }).populate({
            path: 'belongsTo',
            model: 'Division'
        });
        const result = new Promise((resolve, reject) => {
            query.exec((err, doc) => {
                if(err)
                    reject(err);
                resolve(doc);
            });
        });
        return result;
    },
    createProject: async (args:any, owner: any, belongsTo: any): Promise<any> => {
        const newProject = Project.create({
            title: args.title,
            description: args.description,
            state: ProjectStates.planning,
            owner: owner,
            belongsTo: belongsTo,
            techStack: args.techStack,
            members: args.members
        });
        return newProject;
    },
    updateProject: async (doc: any, args: any) => {
        doc.title = args.title ? args.title : doc.title;
        doc.description = args.description ? args.description : doc.description;
        doc.state = args.state ? args.state : doc.state;
        doc.owner = args.owner ? args.owner : doc.owner;
        doc.belongsTo = args.belongsTo ? args.belongsTo : doc.belongsTo;
        doc.techStack = args.techStack ? args.techStack : doc.techStack;
        doc.members = args.members ? args.members : doc.members;
        doc.deadline = args.deadline ? args.deadline : doc.deadline;
        doc.updatedAt = Date.now();
        doc.save();
        return doc;
    },
}