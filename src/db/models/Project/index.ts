import mongoose, { Schema } from "mongoose";

const projectStates = {
    doing: "doing",
    finished: "finished",
    planning: "planning",
}

const ProjectSchema = new Schema({
    title:          String!,
    description:    String,
    owner:          { type: Schema.Types.ObjectId, ref: 'User'},
    belongsTo:      { type: Schema.Types.ObjectId, ref: 'Division'},
    state:          { type: String, default: projectStates.planning },
    techStack:      [{ type: Schema.Types.ObjectId, name: String, ref: 'Skill' }]!,
    members:        [{ type: Schema.Types.ObjectId, name: String, ref: 'User' }]!,
    deadline:       { type: Date },
    createdAt:      { type: Date!, default: Date.now },
    updatedAt:      { type: Date!, default: Date.now }
});

export const ProjectStates = projectStates;
export default mongoose.model('Project', ProjectSchema);