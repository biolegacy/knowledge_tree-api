import mongoose, { Schema } from "mongoose";

const RequestSchema = new Schema({
    title:          String!,
    owner:          { type: Schema.Types.ObjectId, ref: 'User'},
    interest:       [{ type: Schema.Types.ObjectId, name: String, ref: 'Skill' }]!,
    description:    String,
    createdAt:      { type: Date!, default: Date.now },
    updatedAt:      { type: Date!, default: Date.now }
});

export default mongoose.model('Request', RequestSchema);