import User, { UserStatus } from ".";
// Importing util functions
import util from "../../../util";

export default {
    getUsers: async () => {
        const query = User.find().populate({
            path: "companies",
            model: "Company",
            populate: {
                path: "divisions",
                model: "Division"
            }
        });
        const result = new Promise((resolve, reject) => {
            query.exec((err, docs) => {
                if(err)
                    reject(err);
                resolve(docs);
            });
        });
        return result;
    },
    findUserById: async (id: string): Promise<any> => {
        const query = User.findById(id).populate({
            path: "companies",
            model: "Company",
            populate: {
                path: "divisions",
                model: "Division"
            }
        });
        const result = new Promise((resolve, reject) => {
            query.exec((err, doc) => {
                if(err)
                    reject(err);
                resolve(doc);
            })
        });
        return result;
    },
    findUserByEmail: async (email: string): Promise<any> => {
        const query = User.find({ email });
        const result = new Promise((resolve, reject) => {
            query.exec((err, doc) => {
                if(err)
                    reject(err);
                resolve(doc);
            });
        })
        return result;
    },
    createUser: async ( args: any ) => {
        const hashedPassword = await util.hashPassword(args.password);
        const newUser = User.create({
            username: args.username.toLowerCase(),
            email: args.email.toLowerCase(),
            password: hashedPassword,
            companies: [],
        })
        return newUser;
    },
    updateUser: async ( doc: any, args: any, hashedPassword?: string ) => {
        // Update values
        if(args.name !== undefined || args.name !== null){
            doc.name = {
                firstName: args.name.firstName ? args.name.firstName : doc.name.firstName,
                lastName: args.name.lastName ? args.name.lastName : doc.name.lastName,
                firstNameKana: args.name.firstNameKana ? args.name.firstNameKana : doc.name.firstNameKana,
                lastNameKana: args.name.lastNameKana ? args.name.lastNameKana : doc.name.lastNameKana
            }
        }
        doc.password = hashedPassword ? hashedPassword : doc.password;
        doc.phoneNumber = args.phoneNumber ? args.phoneNumber : doc.phoneNumber;
        doc.imageUrl = args.imageUrl ? args.imageUrl : doc.imageUrl;
        doc.userType = args.userType ? args.userType : doc.userType;
        doc.companies = args.companies ? args.companies : doc.companies;
        doc.updatedAt = Date.now();

        doc.save();
        return doc;
    },
    deactivateUser: async (doc: any) => {
        doc.status = UserStatus.inactive;
        doc.deactivatedAt = Date.now();
        doc.updatedAt = Date.now();
        doc.save();
        return doc;
    },
    reactivateUser: async (doc: any) => {
        doc.status = UserStatus.active;
        doc.deactivatedAt = "";
        doc.updatedAt = Date.now();
        doc.save();
        return doc;
    },
    assignCompanyToUser: async (company:any, user: any) => {
        if(Array.isArray(user.company))
            user.company = [];
        user.companies.push(company);
        user.save();
        return user;
    },
    isValidUser: (args: any) => {
        // Check if the email is valid or not
        if(!util.isEmail(args.email))
          throw(new Error('Email is not valid. Please provide a valid email.'));
        // Check if the Password is valid or not
        if(!util.isPasswordValid(args.password))
          throw(new Error("Password is invalid. Please provide proper password."));
        return true;
    }
}