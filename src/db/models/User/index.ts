import mongoose from "mongoose";
const Schema = mongoose.Schema;
const userStatus = {
    active: "active",
    inactive: "inactive"
}

const UserSchema = new Schema({
    username:       String!,
    email:          String!,
    password:       String!,
    phoneNumber:    String,
    name: {
        firstName:      String,
        lastName:       String,
        firstNameKana:  String,
        lastNameKana:   String
    },
    imageUrl:       String,
    userType:       String,
    isVerified:     { type: Boolean, default: false },
    status:         {type: String, default: userStatus.active }, 
    companies:        [{ type: Schema.Types.ObjectId, ref: 'Company'}]!,
    deactivatedAt:  { type: Date! },
    createdAt:      { type: Date!, default: Date.now },
    updatedAt:      { type: Date!, default: Date.now }
});

export const UserStatus = userStatus;
export default mongoose.model('User', UserSchema);