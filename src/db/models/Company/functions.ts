import Company, { CompanyStatus } from ".";

export default {
    getCompanies: async (): Promise<any> => {
        const query = Company.find().populate("divisions");
        const result = new Promise((resolve, reject) => {
            query.exec((err, docs) => {
                if(err)
                    reject(err);
                resolve(docs);
            });
        });
        return result;
    },
    findCompanyById: async (id: string ): Promise<any> => {
        const query = Company.findById(id).populate('divisions');
        const result = new Promise((resolve, reject) => {
            query.exec((err, doc) => {
                if(err)
                    reject(err);
                resolve(doc);
            });
        })
        return result;
    },
    findCompanyByName: async (name: string): Promise<any> => {
        const query = Company.find({ name }).populate('divisions');
        const result = new Promise((resolve, reject) => {
            query.exec((err, doc) => {
                if(err)
                    reject(err);
                resolve(doc);
            })
        })
        return result;
    },
    assignDivisionToCompany: (division: any, company: any ) => {
        if(Array.isArray(company.divisions) !== true)
            // Assign empty array to company divisions if the current divisions is not an Array
            company.divisions = [];
        company.divisions.push(division);
        company.save();
        return company;
    },
    getDivisions: async (id: string): Promise<any> => {
        const query = Company.find({ id });
        const _company = new Promise((resolve, reject) => {
            query.exec((err, doc) => {
                if(err)
                    reject(err);
                resolve(doc);
            });
        })
        return _company;
    },
    createCompany: async (name: string, owner: any): Promise<any> => {

        const newCompany = Company.create({ 
            name,
            owner 
        });
        return newCompany;
    },
    updateCompany: async (doc: any, args: any) => {
        // Update values
        doc.name = args.name;
        doc.description = args.description;
        doc.employees = args.employees;
        doc.divisions = args.divisions;
        doc.updatedAt = Date.now();

        const result = doc.save();
        return result;
    },
    deactivateCompany: async ( doc:any ) => {
        doc.status = CompanyStatus.inactive;
        doc.deactivatedAt = Date.now();
        doc.updatedAt = Date.now();
        doc.save();
        return doc;
    },
    reactivateCompany: async ( doc:any ) => {
        doc.status = CompanyStatus.active;
        doc.deactivatedAt = "";
        doc.updatedAt = Date.now();
        doc.save();
        return doc;
    }
}