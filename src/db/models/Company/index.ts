import mongoose, { Schema } from "mongoose";

const companyStatus = {
    active: "active",
    inactive: "inactive"
}

const CompanySchema = new Schema({
    name:           String!,
    description:    String,
    owner:          { type: Schema.Types.ObjectId, ref: 'User'},
    divisions:      [{ type: Schema.Types.ObjectId, name: String, ref: 'Division' }]!,
    managers:       [{ type: Schema.Types.ObjectId, name: String, ref: 'User' }]!,
    employees:      [{ type: Schema.Types.ObjectId, name: String, ref: 'User' }]!,
    status:         { type: String, default: companyStatus.active},
    deactivatedAt:  { type: Date! },
    createdAt:      { type: Date!, default: Date.now },
    updatedAt:      { type: Date!, default: Date.now }
});

export const CompanyStatus = companyStatus;
export default mongoose.model('Company', CompanySchema);