import mongoose from "mongoose";

export const Init = () => {
    mongoose.connect(`mongodb://localhost:27017/knowledge-tree-local`, { useNewUrlParser: true })
    .then(() => {
        console.log("Connected to MongoDB!");
    })
    .catch((e) => {
        console.log("Error: ", e)
    })

    /*
    mongoose.connection.once('open', () => {
        console.log("Connected to MongoDB!");
    });
    */
}

/* Queries */
