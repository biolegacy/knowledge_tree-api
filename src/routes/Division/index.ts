import { Router } from "express";
import Functions from "../../db/models/Division/functions";

const router:Router = Router();

router.post(`/create`, async (req, res) => {
    let _body = req.body;
    console.log("Incoming Data: ", _body);
    // Check if the current name is available or not
    const divisions: [] = await Functions.findDivisionByName(_body.name);
    if(divisions.length > 0)
        return res.send("Name is already taken!");
    const result = await Functions.createDivisionREST(_body);
    res.send(`Successfully created a division.`);
});

router.get('/', (req, res) => {
    res.send("Division Endpoint!");
})

export default router;