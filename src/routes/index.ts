import {
    Router,
    Request,
    Response
} from "express";
import Division from "./Division";

const router: Router = Router();

router.use('/division', Division);

router.get('/hello', (req, res) => {
    try {
        res.send("Hello");
    } catch(e){
        res.status(400).send("Bad request");
    }
});

export default router;