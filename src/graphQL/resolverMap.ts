import { GraphQLResolveInfo } from 'graphql';
import { Context } from './models';
import { IResolvers } from 'graphql-tools';
// Import Mongoose Models
import User, { UserStatus } from "../db/models/User"; 
import UserFunctions from "../db/models/User/functions";
// Importing Division Model and Functions
import DivisionFunctions from "../db/models/Division/functions";
// Importing Company Model and Functions
import CompanyFunctions from "../db/models/Company/functions";
// Importing Project Model and Functions
import ProjectFunctions from "../db/models/Project/functions";
// Import utility functions 
import util from "../util";
import UserUtils from "../util/user_utils";
import DivisionUtils from "../util/division_utils";
import ProjectUtils from "../util/project_utils";
// Import models
import Division, { DivisionStatus } from '../db/models/Division';
import { CompanyStatus } from '../db/models/Company';
import { ProjectStates } from "../db/models/Project";

const resolverMap: IResolvers = {
  Query: {
    // User related queries
    user: async (_, args) => {
      try{
        const user = await UserFunctions.findUserById(args.id);
        if(!user)
          util.throwError("User doesn't exist");
        return user;
      }catch(e){
        return e;
      }
    },
    users: async () => {
      const result = await UserFunctions.getUsers();
      return result;
    },
    usersByRole: async (role: string) => {

    },
    // Division related queries
    division: async (_, { id }) => {
      const result = await DivisionFunctions.findByDivisionId(id);
      return result;
    },
    divisions: async (_, { id }) => {
      // const result = await DivisionFunctions.getDivisions();
      var result;
      if(!id)
        result = await DivisionFunctions.getDivisions();  
      result = await CompanyFunctions.getDivisions(id);
      
      return id ? result.divisions : result;
    },
    // Company related queries
    companies: async (_, args: any ) => {
      const result = await CompanyFunctions.getCompanies();
      return result;
    },
    company: async (_, args: any) => {
      const result = await CompanyFunctions.findCompanyById(args.id);
      return result;
    },
    // Project related queries
    projects: async (_, args: any) => {
      try{
        let belongsTo;
        if(args && args.id)
          belongsTo = await DivisionFunctions.findByDivisionId(args.id)
        const result = await ProjectFunctions.getProjects(belongsTo ? belongsTo : null);
        return result;
      } catch(e){
        return e;
      }
    },
    project: async (_, args: any) => {
      try{
        const result = await ProjectFunctions.findProjectById(args);
        return result;
      }catch(e){
        return e;
      }
    },
  },
  Mutation:{
    // User related mutations
    createUser: async (_, args ) => {
      try{
        if(UserFunctions.isValidUser(args) !== true)
          throw(new Error("Invalid User"));
        // Check if the email available or not 
        // (E mail must be unique therefore only email only can be linked to one User. 2 users cannot use the same email)
        const doc = await UserFunctions.findUserByEmail(args.email)
        if(doc.length > 0)
          throw(new Error("Email is unavailable. Forgot password?"));
        const newUser = await UserFunctions.createUser(args);
        return newUser;
      }catch(e){
        return e;
      }
    },
    updateUser: async (_, args: any) => {
      try{
        const user = await UserFunctions.findUserById(args.id);
        var hashedPassword;
        if(user.length === 0)
          util.throwError("User not found!");
          // Check if the user is changing his password
        if(args.currentPassword && args.newPassword ){
          // Check if the provided current password matches
          if(util.comparePassword(args.currentPassword, user.password))
          hashedPassword = await util.hashPassword(args.newPassword);
        }
        const updatedUser = await UserFunctions.updateUser(user, args, hashedPassword);
        return updatedUser;
      } catch(e){
        return e;
      }
    },
    deactivateUser: async (_, args: any) => {
      const user = await UserFunctions.findUserById(args.id);
      if(user.length === 0)
        util.throwError("User doesn't exist");
      if(user.status === UserStatus.inactive)
        throw(new Error("User is already inactive."));
      const result = await UserFunctions.deactivateUser(user);
      return result;
    },
    reactivateUser: async (_, args: any) => {
      try{
        const user = await UserUtils.checkIfUserExist(args.id);
        if(user.status !== UserStatus.inactive)
          throw(new Error("User is not inactivate."));
        const result = await UserFunctions.reactivateUser(user);
        return result;
      }catch(e){
        return e;
      }
    },
    // Company related mutations
    createCompany: async (_, args: any ) => {
      try{
        // Find the owner
        const owner = await UserFunctions.findUserById(args.ownerId);
        if(owner.length === 0)
          util.throwError("Owner doesn't exist.");
        const _Result = await CompanyFunctions.findCompanyByName(args.name);
        if(_Result.length > 0){
          return "Name is not available! Company already exists?"
        }
        const newCompany = await CompanyFunctions.createCompany(args, owner);
        // Add company to it's respective owner
        await UserFunctions.assignCompanyToUser(newCompany, owner);
        return newCompany;
      } catch(e){
        return e;
      }
    },
    updateCompany: async (_, args: any) => {
      try{
        const _doc = await CompanyFunctions.findCompanyById(args.id);
        const result = await CompanyFunctions.updateCompany(_doc, args);
        return result;
      } catch(e){
        return e;
      }
    },
    deactivateCompany: async (_, args: any ) => {
      const company = await CompanyFunctions.findCompanyById(args.id);
      if(company.status === CompanyStatus.inactive)
        throw(new Error("The company is already inactive."));
      const result = CompanyFunctions.deactivateCompany(company);
      return result;
    },
    reactivateCompany: async (_, args: any ) => {
      const company = await CompanyFunctions.findCompanyById(args.id);
      if(company.status === CompanyStatus.active)
        throw(new Error("The company is already active."));
      const result = CompanyFunctions.reactivateCompany(company);
      return result;
    },
    // Division related Mutations
    createDivision: async (_, args: any ) => {
      try{
        // Find company that's creating Division
        const Company = await CompanyFunctions.findCompanyById(args.companyId);
        if(Company.length === 0)
          util.throwError("Company doesn't exist.");
        const _Result = await DivisionFunctions.findDivisionByName(args.name);
        if(_Result.length > 0){
          util.throwError('Name is not available! Division already exists?');
        }
        const newDivision = await DivisionFunctions.createDivisionGraphQL(args);
        // Add division to respective company
        await CompanyFunctions.assignDivisionToCompany(newDivision, Company);
        return newDivision;
      } catch(e){
        return e;
      }
    },
    updateDivision: async(_, args: any ) => {
      try{
        const _doc = await DivisionFunctions.findByDivisionId(args.id);
        const result = await DivisionFunctions.updateDivision(_doc, args);
        return result;
      } catch(e){
        return e;
      }
    },
    deactivateDivision: async (_, args) => {
      const division = await DivisionFunctions.findByDivisionId(args.id);
      if(division.status === DivisionStatus.inactive)
        throw(new Error("Division is already inactive."));
      const result = await DivisionFunctions.deactivateDivision(division);
      return result;
    },
    reactivateDivision: async (_, args) => {
      const division = await DivisionFunctions.findByDivisionId(args.id);
      if(division.status === DivisionStatus.active)
        throw(new Error("Division is already active."));
      const result = await DivisionFunctions.reactivateDivision(division);
      return result;
    },
    // Project related mutations
    createProject: async (_, args: any) => {
      // Check if the owner exist
      const user = await UserUtils.checkIfUserExist(args.ownerId);
      const division = await DivisionUtils.checkIfDivisionExist(args.belongsTo);
      const result = await ProjectFunctions.createProject(args, user, division);
      return result;
    },
    updateProject: async (_, args: any) => {
      try{
        const doc = await ProjectFunctions.findProjectById(args);
        const result = await ProjectFunctions.updateProject(doc, args);
        return doc;
      } catch(e){
        return e;
      }
    }
  },
  Holder:{
    __resolveType(content:any , ctx: any, info:any){
        if(content.email !== null)
          return info.schema.getType("User");
        else
          return info.schema.getType("Division");
    }
  }
};

export default resolverMap;
