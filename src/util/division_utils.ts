import DivisionFunctions from "../db/models/Division/functions";

export default {
    checkIfDivisionExist: async (id: string) => {
        const division = await DivisionFunctions.findByDivisionId(id);
        if(division.length === 0 || division === undefined){
            throw(new Error("Division doesn't exist"));
        }
        return division;
    }
}