import ProjectFunctions from "../db/models/Project/functions";

export default {
    checkIfProjectExist: async (id: string) => {
        const project = await ProjectFunctions.findProjectById(id);
        if(project.length === 0 || project === undefined){
            throw(new Error("Project doesn't exist"));
        }
        return project;
    }
}