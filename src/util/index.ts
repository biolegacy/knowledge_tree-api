import bcrypt from "bcrypt";
// Import UserFunctions
import UserFunctions from "../db/models/User/functions";

// Hash salt config
const saltRounds = 10;
// Email Regex
const regex: RegExp = /[a-zA-Z0-9\-]+@[a-zA-Z0-9]+\.[A-Z]{2-4}/;
const EmailRegExp = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);        
// Password Regex
const PasswordRegExp = new RegExp(/^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{6,})/);

export default {
    isEmail: (incValue: string): boolean => {
        const result = EmailRegExp.test(incValue);
        return result;
    },
    hashPassword: async (password: string): Promise<string> => {
        const hashedPassword = bcrypt.hash(password, saltRounds)
        .catch((e) => {
            return e;
        });
        return hashedPassword;
    },
    comparePassword: async (password:string, encryptedPassword: string): Promise<boolean> => {
        return bcrypt.compare(password, encryptedPassword);
    },
    isPasswordValid: (password: string): boolean => {
        const result = PasswordRegExp.test(password);
        return result;
    },
    throwError: (msg: string) => {
        throw(new Error(msg));
    }
}
