import UserFunctions from "../db/models/User/functions";

export default {
    checkIfUserExist: async (id: string) => {
        const user = await UserFunctions.findUserById(id);
        if(user.length === 0 || user === undefined){
            throw(new Error("User doesn't exist"));
        }
        return user;
    }
}