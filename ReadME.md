## How to setup and run the app

    1. npm install
    2. npm run start:dev

    P.S: Check out the npm scripts in package.json file

    #Docker

    Run the app and the DB from the containers, run the following command:
    ( docker-compose -> running the specified commands in written order from docker-compose.yaml )
    1. docker-compose up ( To Start up the app )
    2. docker-compose down ( Shutdown the app )

## Reference guide to configure NodeJS, TypeScript, Apollo, GraphQL server. Use the following link:

    https://medium.com/@th.guibert/basic-apollo-express-graphql-api-with-typescript-2ee021dea2c

## Basic Structure of the APP

    SRC/    - Typescript source code ( TS )
    DIST/   - Compiled source code ( TS -> JS)

## Compile Project

    Run the following command to compile the project. It compiles all of the TS to JS.

    npm run build

## Caution

    When running the npm script command, there might be an issue depending on your OS.
    Need to modify the npm script for your OS if there is an issue.

    Example:
        On Mac:
            "nodemon 'src/server.ts' --exec 'ts-node' src/server.ts -e ts,graphql"
        On Windows:
            "nodemon src/server.ts --exec ts-node src/server.ts -e ts,graphql"

## Mongo Settings

    On Mac: 
    Start MongoDB
    `mongod --config /usr/local/etc/mongod.conf`

    Access to terminal
    `mongo`
