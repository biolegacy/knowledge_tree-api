import { generateText } from './util';

test('Should print out a text', () =>{
    const text = generateText("Edward", 15);
    expect(text).toBe('Edward (15 years old)');
});

test('Should output data-less text', () => {
    const text = generateText("", undefined);
    expect(text).toBe(' ( years old)');
})