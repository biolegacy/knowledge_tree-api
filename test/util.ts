export const generateText = ( name: string | undefined, age: number | undefined ) => {
    return `${name} (${age} years old)`;
}

